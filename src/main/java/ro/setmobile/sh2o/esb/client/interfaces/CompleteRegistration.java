package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ro.setmobile.sh2o.esb.beans.CustomerDetailsBean;

@Path("/CompleteRegistration")
public interface CompleteRegistration {

	@POST
	@Path("/completeRegistration")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String completeRegistration(String customerDetails);

}