package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/GetBaseline")
public interface GetBaseline {

	@GET
	@Path("/getBaseline")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBaseline(@QueryParam("user_id") String user_id);

}