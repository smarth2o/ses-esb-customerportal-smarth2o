package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/{tgt}")
public interface GetServiceTicket {

	@POST	
	@Produces(MediaType.TEXT_PLAIN)
	public String login(@PathParam("tgt") String tgt,String serviceTicket);

}