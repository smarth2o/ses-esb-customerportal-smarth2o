package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/GetNeighbourhood")
public interface GetNeighbourhood {

	@GET
	@Path("/getNeighbourhood")
	@Produces(MediaType.APPLICATION_JSON)
	public String getNeighbourhood(@QueryParam("user_id") String user_id);

}