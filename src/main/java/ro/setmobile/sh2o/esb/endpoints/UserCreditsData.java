/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/UserCreditsData")
public class UserCreditsData {

	@GET
	@Path("/getUserCredits")
	@Produces(MediaType.APPLICATION_JSON)
	public String getUserCredits(@QueryParam("userEmail") String userEmail) {
		return null;
	}
}
