/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import ro.setmobile.sh2o.esb.processor.Sh2oProcessor;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/AlertsData")
public class AlertsData {
	
	static Logger log = Logger.getLogger(Sh2oProcessor.class.getName());
	@GET
	@Path("/getAlerts")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAlerts(@QueryParam("user_id") String user_id) {
		log.info("Log FROM GET ALERTS USERID:"+user_id);
		return null;
	}
}
