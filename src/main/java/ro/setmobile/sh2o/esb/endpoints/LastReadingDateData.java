/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/LastReadingDateData")
public class LastReadingDateData {

	@GET
	@Path("/getLastReadingDate")
	@Produces(MediaType.APPLICATION_JSON)
	public String getLastReadingDate(@QueryParam("user_id") String user_id) {
		return null;
	}
}
