/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/ConsumptionHourlyData")
public class ConsumptionHourlyData {

	@GET
	@Path("/getConsumptionHourly")
	@Produces(MediaType.APPLICATION_JSON)
	public String getConsumption(@QueryParam("user_id") String user_id, @QueryParam("data") String data) {
		return null;
	}
}
