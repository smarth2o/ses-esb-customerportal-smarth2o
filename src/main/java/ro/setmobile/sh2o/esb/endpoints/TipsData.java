/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/ServiceViewSmartH2O/TipsData")
public class TipsData {

	@GET
	@Path("/getTips")
	@Produces(MediaType.APPLICATION_JSON)
	public String getTips(@QueryParam("user_id") String user_id, @QueryParam("lang") String lang) {
		return null;
	}
}
